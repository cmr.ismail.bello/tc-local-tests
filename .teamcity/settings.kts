import jetbrains.buildServer.configs.kotlin.v2019_2.project
import jetbrains.buildServer.configs.kotlin.v2019_2.version
import settings.cleanDockerContainers
import settings.notProdDeploy
import settings.runPythonCI
import settings.webAppBuildAgent

/*
The settings script is an entry point for defining a TeamCity
project hierarchy. The script should contain a single call to the
project() function with a Project instance or an init function as
an argument.

VcsRoots, BuildTypes, Templates, and subprojects can be
registered inside the project using the vcsRoot(), buildType(),
template(), and subProject() methods respectively.

To debug settings scripts in command-line, run the

    mvnDebug org.jetbrains.teamcity:teamcity-configs-maven-plugin:generate

command and attach your debugger to the port 8000.

To debug in IntelliJ Idea, open the 'Maven Projects' tool window (View
-> Tool Windows -> Maven Projects), find the generate task node
(Plugins -> teamcity-configs -> teamcity-configs:generate), the
'Debug' option is available in the context menu for the task.
*/

version = "2020.1"


project {
    description = "Versius Connect Services Group Projects"
    
    subProject {
        id("AuthPlatform")
        name = "Auth Platform"
        description = "CMR Auth Platform"
    }
    
    subProject {
        id("CDPService")
        name = "CDP Service"
        
        buildType {
            id("CDPServiceTest")
            name = "Test"
            
            steps {
                runPythonCI()
                cleanDockerContainers()
            }
            
            requirements {
                // webAppBuildAgent()
                // notProdDeploy()
            }
        }
        
        subProject {
            
            id("CDPServiceDeployment")
            name = "CDP Service Deployment"
            
            buildType {
                id("CDPServiceDeployDev")
                name = "Deploy Dev"
            }
            
            buildType {
                id("CDPServiceDeployStaging")
                name = "Deploy Staging"
            }
            
            buildType {
                id("CDPServiceDeployProd")
                name = "Deploy Prod"
            }
        }
    }
    
    subProject {
        id("InstrumentService")
        name = "Instrument Service"
        
        buildType {
            id("InstrumentServiceTest")
            name = "Test"
            steps {
                runPythonCI()
                cleanDockerContainers()
            }
            
            requirements {
                // webAppBuildAgent()
                // notProdDeploy()
            }
        }
        
        subProject {
            id("InstrumentServiceDeployment")
            name = "Instrument Service Deployment"
            
            buildType {
                id("InstrumentServiceDeployDev")
                name = "Deploy Dev"
            }
            
            buildType {
                id("InstrumentServiceDeployProd")
                name = "Deploy Prod"
            }
        }
    }
}
