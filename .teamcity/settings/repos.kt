package settings

import jetbrains.buildServer.configs.kotlin.v10.vcs.GitVcsRoot

object CdpServiceRepo : GitVcsRoot(
    {
        name = "cdp-serivice"
        url = "git@gitlab.com:cmr-surgical/customer-operations/services/cdp-service/cdp-service.git"
        branch = "develop"
        branchSpec = ":+/refs/heads/(*)"
        authMethod = uploadedKey {
            uploadedKey = "GitLab-Reader"
        }
    }
)


object InstrumentServiceRepo : GitVcsRoot(
    {
        name = "instrument-service"
        url = "git@gitlab.com:cmr-surgical/customer-operations/services/instrument-service/instrument-service.git"
        branch = "develop"
        branchSpec = ":+/refs/heads/(*)"
        authMethod = uploadedKey {
            uploadedKey = "GitLab-Reader"
        }
    }
)