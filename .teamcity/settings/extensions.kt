package settings

import jetbrains.buildServer.configs.kotlin.v2019_2.BuildStep
import jetbrains.buildServer.configs.kotlin.v2019_2.BuildSteps
import jetbrains.buildServer.configs.kotlin.v2019_2.Requirements
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.script


fun BuildSteps.runPythonCI() = script {
    name = "Python CI"
    scriptContent =
        """
        #!/bin/bash

        # Install Poetry
        curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
        # Add poetry to path for first install
        source ${'$'}HOME/.poetry/env

        poetry --version

        (
            [ -e run_python_ci.sh ] && ./run_python_ci.sh || tox
        )
        """.trimIndent()
    param(
        "org.jfrog.artifactory.selectedDeployableServer.downloadSpecSource",
        "Job configuration"
    )
    param(
        "org.jfrog.artifactory.selectedDeployableServer.useSpecs",
        "false"
    )
    param(
        "org.jfrog.artifactory.selectedDeployableServer.uploadSpecSource",
        "Job configuration"
    )
}


fun BuildSteps.cleanDockerContainers() = script {
    name = "Clean docker containers"
    executionMode = BuildStep.ExecutionMode.ALWAYS
    scriptContent =
        """
        #!/bin/bash
        containers=`docker container ls -a -q`
        if [ ${'$'}containers ]
        then
            echo "delete containers"
            docker rm ${'$'}(echo ${'$'}containers)
        fi
        """.trimIndent()
    param(
        "org.jfrog.artifactory.selectedDeployableServer.downloadSpecSource",
        "Job configuration"
    )
    param(
        "org.jfrog.artifactory.selectedDeployableServer.useSpecs",
        "false"
    )
    param(
        "org.jfrog.artifactory.selectedDeployableServer.uploadSpecSource",
        "Job configuration"
    )
}

fun Requirements.webAppBuildAgent() =
    contains("system.agent.name", "CMR-AWS-WEBAPP")

fun Requirements.notProdDeploy() =
    doesNotContain("system.agent.name", "PROD-DEPLOY")

