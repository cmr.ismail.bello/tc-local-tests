# versius connect teamcity settings

This repository contains versius connect information and knowledge TeamCity configuration for deployed services. See [The official documentation](https://www.jetbrains.com/help/teamcity/kotlin-dsl.html) for setup and learning resources. The configuration lives in the `.teamcity` folder of this project, you would be able to run maven goals in this directory.

## External links

* [TeamCity Kotlin DSL official documentation](https://www.jetbrains.com/help/teamcity/kotlin-dsl.html)
* [Kotlin Language documentation](https://kotlinlang.org/?_ga=2.252748809.1289764044.1602851415-1192425676.1602851415)
* [Kotlin DSL Api Documentaion](https://dev.cmedrobotics.com/teamcity-cs/app/dsl-documentation/index.html)

## Development

This guide assumes that you have maven installed on your system. IntelliJ IDEA is the recommended IDE for development.

### Setup

To open the Kotlin DSL project in IntelliJ IDEA, open the `.teamcity/pom.xml` file as a project. All necessary dependencies will be resolved automatically right away. If all dependencies have been resolved, no errors in red will be visible in settings.kts. If you already have an IntelliJ IDEA project. On OSX, the `.teamcity` folder will not be shown by default, use `Command + Shift + .` to display hidden files.

### Building Locally

#### IntelliJ IDEA

On IntelliJ, you can run the Maven goal `teamcity-configs:generate` by clicking Maven -> Plugins -> teamcity-configs -> teamcity-configs:generate. This will confirm your code changes compile and generate a valid configuration that can be loaded into the project.

Please also ensure you run the `ktlint:check` to lint the project in a similar manner.

#### Command Line

On the command line, you can run

``` bash
mvn clean verify teamcity-configs:generate
```

This will clean pervious artifacts, ensure the changes compile and pass `ktlint` checks, and generate the TeamCity project configuration.

## Applying DSL changes

TeamCity periodically polls this repository for changes on `master` and recompiles the DSL. This can be forced in the TeamCity UI as follows:

* Under [Versius Connect/Services -> Edit Project Settings -> Versioned Settings](https://dev.cmedrobotics.com/teamcity-cs/admin/editProject.html?projectId=VersiusConnect_Services&tab=versionedSettings), click `Load project settings from VCS`.

* Confirm you would like to update all settings in the subprojects by clicking `Load settings from VCS`.

This will fetch the latest changes and recompile the project configuration for you.

## Rules of the game

If you are making a small change, you can make changes directly on master or create a MR as you see fit. For more expansive changes, please create a feature branch and ensure your changes are safe before applying on TeamCity.

## FAQ

* How Do I test out my changes on a branch without merging to master?

  * Under [Versius Connect/Services -> Edit Project Settings -> VCS Roots](https://dev.cmedrobotics.com/teamcity-cs/admin/editProject.html?projectId=VersiusConnect_Services&tab=projectVcsRoots), edit the VCS root for this repository to use your branch as the "Default branch". Then follow the steps to re-compile the DSL on teamcity or wait for the latest changes to be picked up as your see fit.

* How do I inspect the changes before applying them on teamcity?

  * The `teamcity-configs:generate` build goal will generate an XML tree in `.teamcity/generated-configs` these can be inspected manually to ensure the changes are as you expect.

* Can I still change settings in the UI?

  * Yes, see [The official documentation](https://www.jetbrains.com/help/teamcity/kotlin-dsl.html) for more information on how this works. Making changes in the UI is still doable however ensure any technical debt that is accumulated because of your "experimentation" is cleaned up on master.

* Where can I find the API documentation?

  * This is the [2019_2 api documentation](https://dev.cmedrobotics.com/teamcity-cs/app/dsl-documentation/jetbrains.build-server.configs.kotlin.v2019_2/index.html), for documentation on earlier versions see the link [here](https://dev.cmedrobotics.com/teamcity-cs/app/dsl-documentation/index.html). Other learning resources are available [here](#external-links)
